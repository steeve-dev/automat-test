/// <reference types="cypress" />

// Welcome to Cypress!
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

beforeEach(() => {
cy.visit('http://localhost:8080/todo')
})


describe('example todo app', () => {
    it('focuses input on load', () => {
    })
})

it('displays two todo items by default', () => {
cy.get('.todo-list li')
.should('have.length', 2)
})

it('get first list element', () => {
    cy.get('.todo-list li').first()
.should('have.text','Pay electric bill')
})

it('get last list element', () => {
    cy.get('.todo-list li').last()
.should('have.text','Walk the dog')
})

it('can add new todo items', () => {
    const newItem = 'Feed the cat'
    cy.get('[data-test=new-todo]').type(`${newItem}{enter}`)
    cy.get('.todo-list li')
.should('have.length', 3)
.last()
.should('have.text', newItem)
})

it('can check off an item as completed', () => {
    cy.contains('Pay electric bill')
    .parent()
    .find('input[type=checkbox]')
    .check()
    cy.contains('Pay electric bill')
    .parents('li')
    .should('have.class', 'completed')
})
    
context('with a checked task', () => {
    beforeEach(() => {
        cy.contains('Pay electric bill')
        .parent()
        .find('input[type=checkbox]')
        .check()
        
            
    })
    it('can filter for uncompleted tasks', () => {
        cy.contains('Active').click()
                cy.get('.todo-list li')
                .should('have.length', 1)
                .first()
                .should('have.text', 'Walk the dog')
                cy.contains('Pay electric bill').should('not.exist')
    })
    it('can filter for completed tasks', () => {
        cy.contains('Completed').click()
        cy.get('.todo-list li')
        .should('have.length', 1)
        .first()
        .should('have.text', 'Pay electric bill')
        cy.contains('Walk the dog').should('not.exist')
    })

    it('can delete all completed tasks', () => {
            cy.contains('Clear completed').click()

            cy.get('.todo-list li')
            .should('have.length', 1)
            .should('not.have.text', 'Pay electric bill')

            cy.contains('Clear completed').should('not.exist')
        })

})

it('can modify existing todo item', () => {
    const modifiedItem = 'Feed the Wookie'
    cy.get('.todo-list li').first() // Récupérer le premier élément de la liste
    .dblclick() // Double clic sur cet élément
    .get('.edit') // Récupération de l'input (classe "edit ")
    .clear() // Suppression de la valeur de l'input
    .type(`${modifiedItem}{enter}`) // Saisie d'une nouvelle valeur + Entrée
    cy.get('.todo-list li')
    .should('have.length', 2)
    .first()
    .should('have.text', modifiedItem)
})


it('can delete existing todo item', () => {
    cy.get('.destroy').first() // Récupération du 1er bouton(classe "destroy")
    .click({ force: true }) // Clic sur le bouton (avec forçage)
    cy.get('.todo-list li')
    .should('have.length', 1)
    .first()
    .should('not.have.text', 'Pay electric bill')
})

it('footer 1, can display numbre of left items', () => {
    cy.get('.todo-count')
    .should('contain', '2 items left')
})

it('footer 2, display number of left items after inserting new item', () => {
    const newItem = 'Feed the cat'
    cy.get('[data-test=new-todo]').type(`${newItem}{enter}`)
    cy.get('.todo-list li')
    .should('have.length', 3)
    .last()
    .should('have.text', newItem)
    cy.get('.todo-count')
    .should('contain', '3 items left')
})

it('footer 3, display number of left items after completing an item', () => {
    cy.contains('Pay electric bill')
    .parent()
    .find('input[type=checkbox]')
    .check()

    cy.get('.todo-count')
    .should('contain', '1 item left')
})

it('footer 4, display number of left items after deleting an item', () => {
    cy.get('.destroy').first() // Récupération du 1er bouton(classe "destroy")
    .click({ force: true }) // Clic sur le bouton (avec forçage)
    
    cy.get('.todo-count')
    .should('contain', '1 item left')

})




beforeEach(() => {
    cy.fixture('todos.json').as('todos')
    cy.visit('http://localhost:8080/todo')
})


it('Add new items from fixtures', function() {
    let nbTodos = 2
    cy.wrap(this.todos)
     .each(todo => {
    const newItem = todo.name
    cy.get('[data-test=new-todo]').type(`${newItem}{enter}`)
    nbTodos = nbTodos+1
    cy.get('.todo-list li')
     .should('have.length', nbTodos)
     .last()
     .should('have.text', newItem)
    })
})